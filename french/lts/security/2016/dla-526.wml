#use wml::debian::translation-check translation="0e9af03d8cf68ff5ddec5d25a056c8dacce03437" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité dans les composants Connector de MySQL d'Oracle
(sous-composant : Connector/J) a été découverte, qui peut avoir pour
conséquence un accès non autorisé à la mise à jour, l'insertion ou la
suppression de certaines données accessibles à des connecteurs MySQL, ainsi
qu'un accès en lecture à un sous-ensemble de données accessibles à des
connecteurs MySQL. Ce problème est corrigé en mettant à jour vers la
dernière version stable de mysql-connector-java dans la mesure où Oracle
n'a pas publié davantage d'informations.</p>

<p>Veuillez consulter les annonces de mises à jour critiques d'Oracle pour
de plus amples détails :</p>

<p><url "http://www.oracle.com/technetwork/topics/security/cpuapr2015verbose-2365613.html#MSQL"></p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 5.1.39-1~deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets
mysql-connector-java.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-526.data"
# $Id: $
