#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été trouvées dans cURL, une URL bibliothèque de
transfert d’URL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000120">CVE-2018-1000120</a>

<p>Duy Phan Thanh a signalé que curl pourrait être dupé pour écrire un octet
vide hors limites lorsqu’il lui était demandé de travailler avec une URL FTP,
avec le réglage pour produire une commande de CWD unique. Le problème pourrait être
déclenché si la partie répertoire de l’URL contenait une séquence « %00 ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000121">CVE-2018-1000121</a>

<p>Dario Weisser a découvert que curl pourrait déréférencer une adresse
proche de NULL lors de la réception d'une URL LDAP. Un serveur malveillant
envoyant une réponse spécialement contrefaite pourrait provoquer le plantage des
applications utilisant les URL LDAP en se reposant sur libcurl.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000122">CVE-2018-1000122</a>

<p>OSS-fuzz et Max Dymond ont trouvé que curl pourrait être entraîné à copier
des données au-delà de la fin de son tampon de tas quand il est appelé
à transférer une URL RTSP. Curl pourrait calculer une mauvaise longueur de données
à copier du tampon de lecture. Cela pourrait conduire à une fuite d'informations
ou à un déni de service.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 7.26.0-1+wheezy25.</p>


<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1309.data"
# $Id: $
