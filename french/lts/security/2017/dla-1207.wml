#use wml::debian::translation-check translation="04b2a712f0dc1f99d41ff1a394028d0309992b19" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un serveur erlang TLS, configuré avec des suites de chiffrement utilisant
l’échange de clefs RSA, peut être vulnérable à une attaque <q>Adaptive Chosen
Ciphertext</q> (alias attaque Bleichenbacher) à l’encontre de RSA, qui,
lorsqu’elle est exploitée, peut aboutir à une récupération en texte pur des
messages chiffrés ou à une attaque d’homme du milieu (MiTM), en dépit que
l’attaquant n’ait pas d’accès à la clef privée du serveur.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 15.b.1-dfsg-4+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets erlang.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1207.data"
# $Id: $
