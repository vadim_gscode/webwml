#use wml::debian::translation-check translation="fb36d8d30ef43fdd50a0075131d4387545f6daaa" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3142">CVE-2017-3142</a>

<p>Un attaquant capable de recevoir et d'envoyer des messages à un serveur
DNS faisant autorité et qui a connaissance du nom d'une clé TSIG valable
peut être capable de contourner l'authentification TSIG de requêtes de
transfert de zone (« AXFR ») à l'aide d'un paquet de requête soigneusement
construit. Un serveur qui se repose uniquement sur des clés TSIG pour sa
protection sans aucune autre protection d'ACL pourrait être manipulé pour :
</p>
<ul>
<li>fournir un transfert de zone à un destinataire non autorisé ;</li>
<li>accepter des paquets NOTIFY corrompus.</li>
</ul></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3143">CVE-2017-3143</a>

<p>Un attaquant capable de recevoir et d'envoyer des messages à un serveur
DNS faisant autorité et qui a connaissance du nom d'une clé TSIG valable
pour la zone et le service ciblés peut être capable de manipuler BIND pour
qu'il accepte une mise à jour dynamique non autorisée. </p></li>
</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1:9.8.4.dfsg.P1-6+nmu2+deb7u17.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1025.data"
# $Id: $
