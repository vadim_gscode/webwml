#use wml::debian::translation-check translation="5357ead7bb298e3857977fea7b0087543c6072b3" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В веб-браузере chromium было обнаружено несколько уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5787">CVE-2019-5787</a>

    <p>Чжэ Цзинь обнаружил использование указателей после освобождения памяти.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5788">CVE-2019-5788</a>

    <p>Марк Бранд обнаружил использование указателей после освобождения памяти в реализации
    FileAPI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5789">CVE-2019-5789</a>

    <p>Марк Бранд обнаружил использование указателей после освобождения памяти в реализации
    WebMIDI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5790">CVE-2019-5790</a>

    <p>Димитрий Фурни обнаружил переполнение буфера в javascript-библиотеке
    v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5791">CVE-2019-5791</a>

    <p>Чунву Хань обнаружил смешение типов в javascript-библиотеке
    v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5792">CVE-2019-5792</a>

    <p>pdknsk обнаружил переполнение целых чисел в библиотеке pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5793">CVE-2019-5793</a>

    <p>Цзюнь Кокатсу обнаружил проблему прав доступа в реализации
    расширений.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5794">CVE-2019-5794</a>

    <p>Джуно Им из Theori обнаружил возможность подделки пользовательского интерфейса.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5795">CVE-2019-5795</a>

    <p>pdknsk обнаружил переполнение целых чисел в библиотеке pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5796">CVE-2019-5796</a>

    <p>Марк Бранд обнаружил состояние гонки в реализации расширений.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5797">CVE-2019-5797</a>

    <p>Марк Бранд обнаружил состояние гонки в реализации DOMStorage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5798">CVE-2019-5798</a>

    <p>Тран Тиен Хун обнаружил чтение за пределами выделенного буфера памяти в библиотеке skia.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5799">CVE-2019-5799</a>

    <p>sohalt обнаружил способ обхода правила безопасности содержимого.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5800">CVE-2019-5800</a>

    <p>Цзюнь Кокатсу обнаружил способ обхода правила безопасности содержимого.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5802">CVE-2019-5802</a>

    <p>Ронни Скансинг обнаружил возможность подделки пользовательского интерфейса.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5803">CVE-2019-5803</a>

    <p>Эндрю Комминов обнаружил способ обхода правила безопасности содержимого.</p></li>

</ul>

<p>В стабильном выпуске (stretch) эти проблемы были исправлены в
версии 73.0.3683.75-1~deb9u1.</p>

<p>Рекомендуется обновить пакеты chromium.</p>

<p>С подробным статусом поддержки безопасности chromium можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4421.data"
