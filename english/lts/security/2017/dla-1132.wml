<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in the Xen hypervisor:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10912">CVE-2017-10912</a>

    <p>Jann Horn discovered that incorrectly handling of page transfers might
    result in privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10913">CVE-2017-10913</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-10914">CVE-2017-10914</a>

    <p>Jann Horn discovered that race conditions in grant handling might
    result in information leaks or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10915">CVE-2017-10915</a>

    <p>Andrew Cooper discovered that incorrect reference counting with
    shadow paging might result in privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10918">CVE-2017-10918</a>

    <p>Julien Grall discovered that incorrect error handling in
    physical-to-machine memory mappings may result in privilege
    escalation, denial of service or an information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10920">CVE-2017-10920</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-10921">CVE-2017-10921</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-10922">CVE-2017-10922</a>

    <p>Jan Beulich discovered multiple places where reference
    counting on grant table operations was incorrect, resulting
    in potential privilege escalation</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12135">CVE-2017-12135</a>

    <p>Jan Beulich found multiple problems in the handling of
    transitive grants which could result in denial of service
    and potentially privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12137">CVE-2017-12137</a>

    <p>Andrew Cooper discovered that incorrect validation of
    grants may result in privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12855">CVE-2017-12855</a>

    <p>Jan Beulich discovered that incorrect grant status handling, thus
    incorrectly informing the guest that the grant is no longer in use.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14316">CVE-2017-14316</a>

    <p>Matthew Daley discovered that the NUMA node parameter wasn't
    verified which which may result in privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14317">CVE-2017-14317</a>

    <p>Eric Chanudet discovered that a race conditions in cxenstored might
    result in information leaks or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14318">CVE-2017-14318</a>

    <p>Matthew Daley discovered that incorrect validation of
    grants may result in a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14319">CVE-2017-14319</a>

    <p>Andrew Cooper discovered that insufficient grant unmapping
    checks may result in denial of service and privilege escalation.</p>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.1.6.lts1-9.</p>

<p>We recommend that you upgrade your xen packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1132.data"
# $Id: $
