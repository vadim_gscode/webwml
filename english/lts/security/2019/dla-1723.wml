<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Various security problems have been discovered in Debian's CRON scheduler.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9525">CVE-2017-9525</a>

    <p>Fix group crontab to root escalation via the Debian package's
    postinst script as described by Alexander Peslyak (Solar Designer) in
    <a href="http://www.openwall.com/lists/oss-security/2017/06/08/3">http://www.openwall.com/lists/oss-security/2017/06/08/3</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9704">CVE-2019-9704</a>

    <p>DoS: Fix unchecked return of calloc(). Florian Weimer discovered that
    a missing check for the return value of calloc() could crash the
    daemon, which could be triggered by a very large crontab created by a
    user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9705">CVE-2019-9705</a>

    <p>Enforce maximum crontab line count of 1000 to prevent a malicious
    user from creating an excessivly large crontab. The daemon will log a
    warning for existing files, and crontab(1) will refuse to create new
    ones.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9706">CVE-2019-9706</a>

    <p>A user reported a use-after-free condition in the cron daemon,
    leading to a possible Denial-of-Service scenario by crashing the
    daemon.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.0pl1-127+deb8u2.</p>

<p>We recommend that you upgrade your cron packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1723.data"
# $Id: $
