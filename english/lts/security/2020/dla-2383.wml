<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in nfdump, a netflow capture daemon.
Both issues are related to either a buffer overflow or an integer
overflow, which could result in a denial of service or a local code
execution.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1.6.15-3+deb9u1.</p>

<p>We recommend that you upgrade your nfdump packages.</p>

<p>For the detailed security status of nfdump please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/nfdump">https://security-tracker.debian.org/tracker/nfdump</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2383.data"
# $Id: $
