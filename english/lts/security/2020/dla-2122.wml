<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that libusbmuxd incorrectly handled socket
permissions. A remote attacker could use this issue to access
services on iOS devices, contrary to expectations.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.0.9-1+deb8u1.</p>

<p>We recommend that you upgrade your libusbmuxd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2122.data"
# $Id: $
