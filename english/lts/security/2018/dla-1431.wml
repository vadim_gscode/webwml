<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>unzip and untar target tasks in ant allows the extraction of files
outside the target directory. A crafted zip or tar file submitted to
an Ant build could create or overwrite arbitrary files with the
privileges of the user running Ant.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.9.4-3+deb8u1.</p>

<p>We recommend that you upgrade your ant packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1431.data"
# $Id: $
