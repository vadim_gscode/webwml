#use wml::debian::template title="Quem está usando o Debian?" BARETITLE=true
#use wml::debian::toc
#use wml::debian::users_list
#use wml::debian::translation-check translation="257c21bc3adb2b3c7174439a605ea9010623492d"
# $Id$

<p>
  Aqui estão incluídas descrições de algumas organizações
  que vêm usando o Debian e que enviaram um pequeno relato de como
  o utilizam e de porque o escolheram. As entradas estão listadas
  alfabeticamente. Se você gostaria de ser incluído(a) nesta lista, por
  favor <a href="#submissions">siga estas instruções</a>.
</p>

<toc-display />

<toc-add-entry name="edu">Instituições educacionais</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'edu', '.*') :>

<toc-add-entry name="com">Comercial</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'com', '.*') :>

<toc-add-entry name="org">Organizações sem fins lucrativos</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'org', '.*') :>

<toc-add-entry name="gov">Organizações governamentais</toc-add-entry>
<:= get_users_list ('$(ENGLISHDIR)/users', 'gov', '.*') :>

<hr />

<h2><a name="submissions" id="submissions">Submissões</a></h2>
<p>
  Para ser adicionado a esta lista, por favor inclua as seguintes informações
  em um e-mail para
  <a href="mailto:debian-www@lists.debian.org?subject=Who's%20using%20Debian%3F">debian-www@lists.debian.org</a>.
  (por favor, mantenha o assunto <q>Who's using Debian</q> (Quem está usando o
  Debian), senão podemos não notar sua submissão).
  As submissões devem ser enviadas em inglês. Se você não fala inglês,
  por favor envie sua submissão para a
  <a href="https://lists.debian.org/i18n.html">lista de traduções apropriada</a>.
  Tenha em mente que seu e-mail de submissão ficará disponível em uma lista de
  discussão pública.
</p>

<p>
  Somente envie informações sobre organizações das quais você é um(a)
  representante. Você poderá ter que pedir autorização a sua organização antes
  de fazer uma submissão aqui. Se você é um(a) consultor(a) e parte da sua
  remuneração é proveniente de suporte <strong>pago</strong> ao Debian, por
  favor direcione-se a nossa página de
  <a href="$(HOME)/consultants/">Consultores(as)</a>.
</p>

<p>
  Por favor, não envie informações sobre indivíduos privados. Nós respeitamos
  nossos(as) apoiadores(as), entretanto não temos pessoal suficiente para manter
  informações sobre tantos(as) usuários(as) particulares.
</p>

<p>
  <strong>Somente inclua informações que você acredita que são adequadas para
  conhecimento público.</strong>
</p>

<ol>
  <li>
    <p>
      Nome da organização (na forma de <em>divisão</em>, <em>organização</em>,
      <em>cidade</em> (opcional), <em>região/província</em> (opcional),
      <em>país</em>).
      Um exemplo: AI Lab, Instituto de Tecnologia de Massachusetts, EUA
    </p>
    <p>
      Nem todas as entradas estão neste formato, mas nós gostaríamos de
      converter aquelas que não estão.
    </p>
  </li>
  <li>Tipo de organização (escolha somente uma dentre <q>educacional</q>,
  <q>sem fins lucrativos</q>, <q>comercial</q> ou <q>governamental</q>)</li>
  <li>Link para página web <em>(opcional)</em></li>
  <li>Um parágrafo ou dois, curtos, descrevendo como sua organização usa o
    Debian. Tente incluir detalhes como o número de estações de
    trabalho/servidores, os softwares que eles rodam (não é preciso detalhar as
    versões) e por que você escolheu o Debian em vez dos concorrentes.
  </li>
</ol>

<p>
  Se você representa uma organização, considere tornar-se um(a) Parceiro(a)
  Debian. Por favor, veja nosso
  <a href="$(HOME)/partners/">Programa de Parceiros(as)</a> para mais
  informações sobre como prover assistência de longo prazo ao Debian. Para
  doações ao Debian, por favor veja nossa página de
  <a href="$(HOME)/donations/">Doações</a>.
</p>
