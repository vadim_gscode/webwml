<define-tag pagetitle>Debian Edu / Skolelinux Buster — en komplett Linuxlösning för din skola</define-tag>
<define-tag release_date>2019-07-07</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="df8b497185c1a26f20b79e08fe7543e9c4d55353" maintainer="Andreas Rönnquist"
<p>
Behöver du administrera datorlabb eller hela skolnätverk?
Skulle du vilja installera servrar, arbetsstationer och laptops som
sedan kommer att arbeta tillsammans?
Vill du ha Debians stabilitet med nätverkstjänster redan förkonfigurerade?
Vill du ha ett webbaserat verktyg för att hantera system och hundratals
eller till och med ännu fler användarkonton?
Har du frågat dig själv om och hur gamla datorer kan användas?
</p>

<p>
I sådana fall är Debian Edu för dig. Lärarna själva eller deras tekniska
support kan rulla ut en komplett multimaskinsstudiemiljö för flera användare på
bara några dagar. Debian Edu kommer med hundratals program förinstallerade,
men du kan alltid lägga till fler program från Debian.
</p>

<p>
Utvecklargruppen bakom Debian Edu tillkännager stolt Debian Edu 10
<q>Buster</q>, Debian Edu- / Skolelinuxutgåvan som baseras
på utgåvan Debian 10 <q>Buster</q>.
Vänligen testa den och rapportera till (&lt;debian-edu@lists.debian.org&gt;)
för att hjälpa oss att ytterligare förbättra den.
</p>

<h2>Om Debian Edu och Skolelinux</h2>

<p>
<a href="https://wiki.debian.org/DebianEdu"> Debian Edu, även känd som
Skolelinux</a>, är en Linuxdistribution baserad på Debian som tillhandahåller
en out-of-the-box-miljö för ett fullständigt konfigurerat skolnätverk.
Direkt efter att du har installerat en skolserver som kör alla tjänster
som behövs för ett skolnätverk är det redo och väntar på användare och
maskiner som läggs till via GOsa², ett bekvämt webbgränssnitt. En
nätstartsmiljö förbereds, så efter den första installationen av
huvudservern från CD / DVD / BD eller USB-minne kan alla andra maskiner
installeras via nätverket.
Äldre datorer (till och med upp till runt 10 år gamla) kan användas som
LTSP - tunna klienter eller disklösa arbetsstationer, som startar från
nätverket utan någon installation eller några inställningar alls.
skolservern Debian Edu tillhandahåller en LDAP-databas och
autentiseringstjänsten Kerberos, centraliserade hemkataloger, en DHCP-server,
en webbproxy och många andra tjänster. Skrivbordet innehåller mer än 60
pedagogiska mjukvarupaket och fler finns tillgängliga i Debianarkivet.
Skolor kan välja mellan skrivbordsmiljöerna Xfce, GNOME, LXDE, MATE,
KDE Plasma och LXQt.
</p>

<h2>Nya funktioner för Debian Edu 10 <q>Buster</q></h2>

<p>Följande är några punkter från versionsfakta för Debian Edu 10 <q>Buster</q>,
baserad på utgåvan Debian 10 <q>Buster</q>.
Den fullständiga listan som inkluderar mer detaljerad information är en
del av den relaterade <a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Features#New_features_in_Debian_Edu_Buster">Debian Edu manualen</a>.
</p>

<ul>
<li>
Officiella Debianinstallationsavbildningar finns nu tillgängliga.
</li>
<li>
Sajt-specifik modulär installation är möjlig.
</li>
<li>
Ytterligare meta-paket som grupperar pedagogiska paket efter skolnivå
tillhandahålls.
</li>
<li>
Förbättrad skrivbordslokalisering för alla språk som Debian stödjer.
</li>
<li>
Verktyg tillgängliga för att förenkla inställning av sajtspecifikt
multispråkstöd.
</li>
<li>
Tillagt löseordshantering för GOsa²-insticksmodulen.
</li>
<li>
Förbättrat TLS/SSL-stöd inuti det interna nätverket.
</li>
<li>
Kerberos-setupen stödjer NFS och SSH-tjänster.
</li>
<li>
Ett verktyg för att återgenerera LDAP-databasen finns tillgängligt.
</li>
<li>
X2Go-servern installeras på alla system med profil-LTSP-Server.
</li>
</ul>

<h2>Hämtningsalternativ, installationssteg samt manual</h2>

<p>
Separata CD-avbildningar för Nätverksinstallation för 64-bitars och
32-bitars PCs finns tillgängliga. 32-bitarsavbildningen är endast nödvändig
i sällsynta fall (för PCs som är äldre än runt 12 år). Avbildningarna kan
hämtas från följande platser:
</p>
<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Alternativt finns utökade BD-avbildningar (större än 5 GB) tillgängliga.
Det är möjligt att sätta upp ett fullständigt Debian Edu-nätverk utan en
internetanslutning (alla skrivbordsmiljöer med stöd, alla språk som stöds av
Debian). Dessa avbildningar kan hämtas på följande platser:
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
Avbildningarna kan verifieras med hjälp av signerade kontrollsummor som
tillhandahålls i hämtningsmappen.
<br />
När du har hämtat en avbildning kan du kontrollera att
</p>

<ul>
<li>
dess kontrollsumma matchar den förväntade från filen checksum; och att
</li>
<li>
filen checksum inte har manipulerats.
</li>
</ul>

<p>
För ytterligare information om hur man utför dessa steg, läs
<a href="https://www.debian.org/CD/verify">verifieringsguiden</a>.
</p>

<p>
Debian Edu 10 <q>Buster</q> baseras fullständigt på Debian 10 <q>Buster</q>;
så källkoden för alla paket finns tillgänglig från Debianarkivet.
</p>

<p>
Vänligen se
<a href="https://wiki.debian.org/DebianEdu/Status/Buster">statussidan för Debian Edu Buster</a>
för uppdaterad information om Debian Edu 10 <q>Buster</q> inklusive instruktioner
för hur du använder <code>rsync</code> för att hämta ISO-avbildningarna.
</p>

<p>
När du uppgraderar från Debian Edu 9 <q>Stretch</q> var vänlig läs det
relaterade
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Upgrades">kapitlet i Debian Edu-manualen</a>.
</p>

<p>
För installationsanvisningar vänligen se det relaterade
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Installation#Installing_Debian_Edu">kapitlet i Debian Edu-manualen</a>.
</p>

<p>
Efter installationen måste du ta dessa
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/GettingStarted">första steg</a>.
</p>

<p>
Vänligen se <a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/">Debian Edu's wikisidor</a>
för den senaste engelska versionen av Debian Edu <q>Buster</q>-manualen.
Manualen har kompletta översättningar till tyska, franska, italienska, danska,
holländska, norskt bokmål och japanska. Delvis översatta versioner finns för
spanska och förenklad kinesiska.
En översikt över <a href="https://jenkins.debian.net/userContent/debian-edu-doc/">
de senast översatta versionerna av manualen</a> finns tillgänglig.
</p>

<p>
Mer information om Debian 10 <q>Buster</q> självt kommer att tillhandahållas i
versionsfakta och installationsmanualen; se <a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>Om Debian</h2>

<p>
	Debianprojektet är en sammanslutning av utvecklare av fri mjukvara som
	ger frivilligt av sin tid och insats för att producera det helt fria
	operativsystemet Debian.
</p>


<h2>Kontaktinformation</h2>

<p>För ytterligare information, var vänlig besök Debians webbplats på
<a href="$(HOME)/">http://www.debian.org/</a> eller skicka e-post till
&lt;press@debian.org&gt;.</p>

